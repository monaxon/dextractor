export interface State {
  countDownloadableFiles: number;
  last: boolean;
  possibleFileExtensions: string[];
}

export type CallBack = (links?: string[]) => void;

export type StartFunction = (
  link: string,
  mode: number,
  path: string,
  callback: CallBack
) => Promise<string | string[] | undefined>;

export interface Action {
  start: StartFunction;
}

export type SaveLinksAndDownloadFilesMethods = (
  url: string,
  path?: string,
  callback?: CallBack
) => Promise<string>;

export type GetLinksMethod = (
  url: string,
  callback?: CallBack
) => Promise<string[]>;
