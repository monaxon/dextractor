import instanceOfActions from "./actions";
import {
  CallBack,
  GetLinksMethod,
  SaveLinksAndDownloadFilesMethods,
} from "./types";

/**
 * Asynchronously saves the links that appear inside a given URL in text file in the given path and downloads the files in the links that actually end to a file.
 * It either resolves to `"Done!"` or it gets rejected to a string in which contains the error message.
 * @param url The URL that you wish the dextraction to be performed on.
 * @param path The path that you wish the downloaded files and the saved links file to be stored in. Default is `"./export/"`.
 * @param callback The function you wish to be executed right after the dextraction is done. The default callback function logs `"Done!"` to the console.
 */
export const saveLinksAndDownloadFiles: SaveLinksAndDownloadFilesMethods = (
  url: string,
  path = "./export/",
  callback: CallBack = () => {
    console.log("Done!");
  }
) => {
  return new Promise<string>((resolve, reject) => {
    instanceOfActions.start(url, 0, path, callback).then((error) => {
      if (error) {
        reject(error.toString());
      } else {
        resolve("Done!");
      }
    });
  });
};

/**
 * Asynchronously saves the links that appear inside a given URL in text file in the given path.
 * It either resolves to `"Done!"` or it gets rejected to a string in which contains the error message.
 * @param url The URL that you wish the dextraction to be performed on.
 * @param path The path that you wish the downloaded files and the saved links file to be stored in. Default is `"./export/"`.
 * @param callback The function you wish to be executed right after the dextraction is done. The default callback function logs `"Done!"` to the console.
 */
export const saveLinks: SaveLinksAndDownloadFilesMethods = (
  url: string,
  path = "./export/",
  callback: CallBack = () => {
    console.log("Done!");
  }
) => {
  return new Promise<string>((resolve, reject) => {
    instanceOfActions.start(url, 1, path, callback).then((error) => {
      if (error) {
        reject(error.toString());
      } else {
        resolve("Done!");
      }
    });
  });
};

/**
 * Asynchronously downloads the files in the links that actually end to a file in the given path.
 * It either resolves to `"Done!"` or it gets rejected to a string in which contains the error message.
 * @param url The URL that you wish the dextraction to be performed on.
 * @param path The path that you wish the downloaded files and the saved links file to be stored in. Default is `"./export/"`.
 * @param callback The function you wish to be executed right after the dextraction is done. The default callback function logs `"Done!"` to the console.
 */
export const downloadFiles: SaveLinksAndDownloadFilesMethods = function (
  url: string,
  path = "./export/",
  callback: CallBack = () => {
    console.log("Done!");
  }
) {
  return new Promise<string>((resolve, reject) => {
    instanceOfActions.start(url, 2, path, callback).then((error) => {
      if (error) {
        reject(error.toString());
      } else {
        resolve("Done!");
      }
    });
  });
};

/**
 * Asynchronously returns the links that appear inside a given URL.
 * It either resolves to an array of strings or it gets rejected to a string in which contains the error message.
 * You can either use the returned links as a parameter to the callback function, or do as you wish with the promise.
 * @param url The URL that you wish the dextraction to be performed on.
 * @param callback The function you wish to be executed right after the dextraction is done. The default callback function is empty and it accepts a `links` parameter which is the array of returned links.
 */
export const getLinks: GetLinksMethod = function (
  url: string,
  callback: CallBack = () => {
    // empty callback
  }
) {
  return new Promise<string[]>((resolve, reject) => {
    instanceOfActions.start(url, 3, "./", callback).then((result) => {
      if (typeof result === "object") {
        resolve(result);
      } else {
        reject(result);
      }
    });
  });
};
