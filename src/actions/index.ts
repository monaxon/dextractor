import { Action, CallBack } from "../types";
import parseLink from "./private/parseLink";
import getBodyOfLink from "./private/getBodyOfLink";
import getHrefs from "./private/getHrefs";
import doTheJob from "./private/doTheJob";

const Actions = (function (this: Action): void {
  this.start = async function (
    link: string,
    mode: number,
    path = "./",
    callback: CallBack
  ) {
    return new Promise<string | string[] | undefined>((resolve) => {
      const parsedURL = parseLink(link);
      const parsedPath = parseLink(path);
      getBodyOfLink(parsedURL)
        .then((body) => {
          const myHREFs: RegExpMatchArray = getHrefs(body);
          const links: Promise<string[] | undefined> = doTheJob(
            parsedURL,
            parsedPath,
            myHREFs,
            mode,
            callback
          );
          resolve(links);
        })
        .catch((error) => {
          const errorString = String(error);
          resolve(errorString);
        });
    });
  };
} as unknown) as { new (): Action };
const instanceOfActions = new Actions();
Object.freeze(instanceOfActions);
export default instanceOfActions;
