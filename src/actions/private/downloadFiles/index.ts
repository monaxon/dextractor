import fs from "fs";
import requestPromise from "request-promise";
import { CallBack } from "../../../types";
import myStates from "../../../states";
import isFile from "../isFile";
import formatPath from "../formatPath";
import archiveFiles from "../archiveFiles";

const downloadFiles = (
  trimmedLink: string,
  parsedURL: string,
  parsedPath: string,
  callback: CallBack = () => {
    // empty callback
  }
) => {
  return new Promise<boolean>((resolve) => {
    const arrayOfTrimmedLink = trimmedLink.split("/");
    if (
      // Check if it is a file
      isFile(arrayOfTrimmedLink[arrayOfTrimmedLink.length - 1])
    ) {
      myStates.countDownloadableFiles++;
      console.log(
        `The file ${
          arrayOfTrimmedLink[arrayOfTrimmedLink.length - 1]
        } started downloading...`
      );
      if (!fs.existsSync(`${parsedPath}${formatPath(parsedURL)}/files/`)) {
        fs.mkdirSync(`${parsedPath}${formatPath(parsedURL)}/files/`, {
          recursive: true,
        });
      }
      const result = requestPromise.get({
        uri: trimmedLink,
        headers: {
          Accept:
            "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
          "Accept-Encoding": "gzip, deflate, br",
          "Accept-Language":
            "en-US,en;q=0.9,fr;q=0.8,ro;q=0.7,ru;q=0.6,la;q=0.5,pt;q=0.4,de;q=0.3",
          "Cache-Control": "max-age=0",
          Connection: "keep-alive",
          "Upgrade-Insecure-Requests": "1",
          "User-Agent":
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36",
        },
      });
      result
        .then(async (value) => {
          const file: fs.WriteStream = fs.createWriteStream(
            parsedPath +
              formatPath(parsedURL) +
              `/files/${arrayOfTrimmedLink[arrayOfTrimmedLink.length - 1]}`
          );
          file.write(value);
          console.log(
            `The file ${
              arrayOfTrimmedLink[arrayOfTrimmedLink.length - 1]
            } has finished downloading.`
          );
          myStates.countDownloadableFiles--;
          if (myStates.countDownloadableFiles === 0 && myStates.last) {
            await archiveFiles(parsedURL, parsedPath, () => {
              resolve(true);
              callback();
            });
          } else {
            resolve(false);
          }
        })
        .catch(async (error) => {
          console.error(error);
          myStates.countDownloadableFiles--;
          if (myStates.countDownloadableFiles === 0 && myStates.last) {
            await archiveFiles(parsedURL, parsedPath, () => {
              resolve(true);
              callback();
            });
          } else {
            resolve(false);
          }
        });
    } else {
      if (myStates.countDownloadableFiles === 0 && myStates.last) {
        archiveFiles(parsedURL, parsedPath, () => {
          resolve(true);
          callback();
        });
      } else {
        resolve(false);
      }
    }
  });
};

export default downloadFiles;
