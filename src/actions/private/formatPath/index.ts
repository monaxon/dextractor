const formatPath = (parsedURL: string): string => {
  return parsedURL.replace(/\//g, "_").replace(/\:/g, "_");
};

export default formatPath;
