const getHrefs = (body: string): RegExpMatchArray => {
  const myMatchArray = body.match(/href\=\"[^"]+\"/gm);
  if (myMatchArray) {
    const myHREFs: RegExpMatchArray = myMatchArray;
    return myHREFs;
  } else {
    const myHREFs: RegExpExecArray = ([] as never) as RegExpExecArray;
    return myHREFs;
  }
};

export default getHrefs;
