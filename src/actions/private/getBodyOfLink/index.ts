import request from "request-promise";

const getBodyOfLink = (parsedURL: string) => {
  return new Promise<string>(async (resolve, reject) => {
    await request.get(parsedURL, async (error, _response, body) => {
      if (error) {
        const errorString = String(error);
        reject(errorString);
      } else {
        const bodyString = String(body);
        resolve(bodyString);
      }
    });
  });
};

export default getBodyOfLink;
