import fs from "fs";
import formatPath from "../formatPath";

const deleteLinksIfExist = (
  parsedURL: string,
  parsedPath: string,
  callback = () => {
    // empty callback
  }
) => {
  if (fs.existsSync(`${parsedPath}${formatPath(parsedURL)}/links/`)) {
    fs.rmdirSync(`${parsedPath}${formatPath(parsedURL)}/links/`, {
      recursive: true,
    });
    callback();
  } else {
    callback();
  }
};

export default deleteLinksIfExist;
