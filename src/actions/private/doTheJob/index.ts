import { CallBack } from "../../../types";
import deleteLinksIfExist from "../deleteLinksIfExist";
import trimLink from "../trimLink";
import saveLink from "../saveLink";
import downloadFiles from "../downloadFiles";
import myStates from "../../../states";

const doTheJob = (
  parsedURL: string,
  parsedPath: string,
  myHREFs: RegExpMatchArray,
  mode: number,
  callback: CallBack = () => {
    // empty callback
  }
) => {
  return new Promise<string[] | undefined>(async (resolve) => {
    if (mode === 0 || mode === 1) {
      deleteLinksIfExist(parsedURL, parsedPath, async () => {
        for (let i = 0; i < myHREFs.length; i++) {
          const trimmedLink = trimLink(myHREFs[i], parsedURL);
          if (i === myHREFs.length - 1 && mode !== 0) {
            saveLink(trimmedLink, parsedURL, parsedPath, callback);
            resolve(undefined);
          } else {
            saveLink(trimmedLink, parsedURL, parsedPath);
          }
          if (mode !== 1) {
            if (i === myHREFs.length - 1) {
              myStates.last = true;
            }
            const downloadResult = await downloadFiles(
              trimmedLink,
              parsedURL,
              parsedPath,
              callback
            );
            if (downloadResult) {
              resolve(undefined);
            }
          }
        }
      });
    } else if (mode === 2) {
      for (let i = 0; i < myHREFs.length; i++) {
        const trimmedLink = trimLink(myHREFs[i], parsedURL);
        if (i === myHREFs.length - 1) {
          myStates.last = true;
        }
        const downloadResult = await downloadFiles(
          trimmedLink,
          parsedURL,
          parsedPath,
          callback
        );
        if (downloadResult) {
          resolve(undefined);
        }
      }
    } else if (mode === 3) {
      const links = [];
      for (let i = 0; i < myHREFs.length; i++) {
        const trimmedLink = trimLink(myHREFs[i], parsedURL);
        links.push(trimmedLink);
        if (links.length === myHREFs.length) {
          callback(links);
          resolve(links);
        }
      }
    }
  });
};

export default doTheJob;
