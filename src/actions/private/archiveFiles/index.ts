import fs from "fs";
import { zip } from "zip-a-folder";
import { CallBack } from "../../../types";
import formatPath from "../formatPath";

const archiveFiles = async (
  parsedURL: string,
  parsedPath: string,
  callback: CallBack = () => {
    // empty callback
  }
) => {
  const zipResult = await zip(
    `${parsedPath}${formatPath(parsedURL)}/files/`,
    `${parsedPath}${formatPath(parsedURL)}/files.zip`
  );
  if (zipResult) {
    console.error(zipResult);
    callback();
  } else {
    console.log("Downloaded files are zipped.");
    fs.rmdirSync(`${parsedPath}${formatPath(parsedURL)}/files/`, {
      recursive: true,
    });
    callback();
  }
};

export default archiveFiles;
