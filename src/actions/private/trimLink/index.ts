const trimLink = (href: string, parsedURL: string): string => {
  const trimming =
    href.split('"')[1].substr(0, 1) === "/"
      ? href.split('"')[1].substr(1, href.split('"')[1].length - 1)
      : href.split('"')[1];
  const trimmedLink =
    trimming.search(/https\:\/\//g) === -1 &&
    trimming.search(/http\:\/\//g) === -1 &&
    trimming.search(/www\./g) === -1
      ? parsedURL + trimming
      : trimming;
  return trimmedLink;
};

export default trimLink;
