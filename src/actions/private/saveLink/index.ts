import fs from "fs";
import { CallBack } from "../../../types";
import formatPath from "../formatPath";

const saveLink = (
  trimmedLink: string,
  parsedURL: string,
  parsedPath: string,
  callback?: CallBack
) => {
  const underlineSlashedURL = formatPath(parsedURL);
  if (!fs.existsSync(`${parsedPath}${underlineSlashedURL}/links/links.txt`)) {
    fs.mkdirSync(`${parsedPath}${underlineSlashedURL}/links/`, {
      recursive: true,
    });
  }
  fs.writeFileSync(
    `${parsedPath}${underlineSlashedURL}/links/links.txt`,
    `${trimmedLink}\n`,
    { flag: "a" }
  );
  if (callback) {
    callback();
  }
};

export default saveLink;
