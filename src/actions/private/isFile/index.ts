import myStates from "../../../states";

const isFile = (name: string): boolean => {
  const splited = name.toUpperCase().split(/\./gi);
  return splited.length > 1
    ? myStates.possibleFileExtensions.includes(splited[splited.length - 1])
    : false;
};

export default isFile;
