const parseLink = (link: string): string => {
  const parsedURL = link.substr(link.length - 1) === "/" ? link : `${link}/`;
  return parsedURL;
};

export default parseLink;
