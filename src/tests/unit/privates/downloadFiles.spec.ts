import downloadFiles from "../../../actions/private/downloadFiles";
import archiveFiles from "../../../actions/private/archiveFiles";
import mockData from "../mocks/privates.mock";
import fs from "fs";
import request from "request-promise";

jest.mock("fs");
jest.mock("request-promise");
jest.mock("../../../actions/private/archiveFiles");
jest.mock("console");

describe("Private method: downloadFiles()", () => {
  it("Should download file", async () => {
    (archiveFiles as jest.Mock).mockImplementation(() => {
      // empty
    });
    const spyOnExistsSync = jest.spyOn(fs, "existsSync");
    const spyOnMkdirSync = jest.spyOn(fs, "mkdirSync");
    const spyOnCreateWriteStream = jest.spyOn(fs, "createWriteStream");
    const spyOnConsoleLog = jest
      .spyOn(global.console, "log")
      .mockImplementation((text: string) => {
        return text;
      });
    const spyOnConsoleError = jest
      .spyOn(global.console, "error")
      .mockImplementation((text: string) => {
        return text;
      });
    const spyOnRequestGet = jest.spyOn(request, "get");
    spyOnRequestGet.mockResolvedValue(true);

    (fs.existsSync as jest.Mock).mockReturnValue(false);

    // const mockRequest = await request.get({
    //   uri: `${mockData.parsedURL.toBe.httpsWWW}${mockData.fileName}`,
    // });
    expect(spyOnExistsSync).not.toHaveBeenCalled();
    expect(spyOnMkdirSync).not.toHaveBeenCalled();
    expect(spyOnCreateWriteStream).not.toHaveBeenCalled();
    expect(spyOnConsoleLog).not.toHaveBeenCalled();
    expect(spyOnConsoleError).not.toHaveBeenCalled();
    expect(spyOnRequestGet).not.toHaveBeenCalled();
    await downloadFiles(
      `${mockData.parsedURL.toBe.httpsWWW}${mockData.fileName}`,
      mockData.parsedURL.httpsWWW,
      mockData.path
    );
    expect(spyOnExistsSync).toHaveBeenCalled();
    expect(spyOnMkdirSync).toHaveBeenCalled();
    expect(spyOnCreateWriteStream).toHaveBeenCalled();
    expect(spyOnConsoleLog).toHaveBeenCalled();
    expect(spyOnConsoleError).toHaveBeenCalled();
    expect(spyOnRequestGet).toHaveBeenCalled();
  });

  afterEach(() => {
    jest.clearAllMocks();
    jest.restoreAllMocks();
  });
});
