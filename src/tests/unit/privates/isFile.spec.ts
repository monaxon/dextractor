import isFile from "../../../actions/private/isFile";
import mockData from "../mocks/privates.mock";

describe("Private method: isFile()", () => {
  it("Should return true", () => {
    const result = isFile(mockData.fileName);
    return expect(result).toBeTruthy();
  });
  it("Should return false", () => {
    const result = isFile("Dummy string");
    return expect(result).toBeFalsy();
  });
});
