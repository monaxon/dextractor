import saveLink from "../../../actions/private/saveLink";
import mockData from "../mocks/privates.mock";
import fs from "fs";

jest.mock("fs");

describe("Private method: saveLink()", () => {
  it("Makes the directory and the file", () => {
    const spyOnExistsSync = jest.spyOn(fs, "existsSync");
    const spyOnMkdirSync = jest.spyOn(fs, "mkdirSync");
    const spyOnWriteFileSync = jest.spyOn(fs, "writeFileSync");

    expect(spyOnExistsSync).not.toHaveBeenCalled();
    expect(spyOnMkdirSync).not.toHaveBeenCalled();
    expect(spyOnWriteFileSync).not.toHaveBeenCalled();

    (fs.existsSync as jest.Mock).mockReturnValue(false);

    saveLink(
      mockData.parsedURL.toBe.httpsWWW,
      mockData.parsedURL.httpsWWW,
      "./export/"
    );

    expect(spyOnExistsSync).toBeCalledWith(
      `./export/${mockData.underlineSlashed}/links/links.txt`
    );
    expect(spyOnMkdirSync).toBeCalledWith(
      `./export/${mockData.underlineSlashed}/links/`,
      {
        recursive: true,
      }
    );
    expect(spyOnWriteFileSync).toBeCalledWith(
      `./export/${mockData.underlineSlashed}/links/links.txt`,
      `${mockData.parsedURL.toBe.httpsWWW}\n`,
      {
        flag: "a",
      }
    );
  });
  it("Appends only the file", () => {
    const spyOnExistsSync = jest.spyOn(fs, "existsSync");
    const spyOnMkdirSync = jest.spyOn(fs, "mkdirSync");
    const spyOnWriteFileSync = jest.spyOn(fs, "writeFileSync");

    expect(spyOnExistsSync).not.toHaveBeenCalled();
    expect(spyOnMkdirSync).not.toHaveBeenCalled();
    expect(spyOnWriteFileSync).not.toHaveBeenCalled();

    (fs.existsSync as jest.Mock).mockReturnValue(true);

    saveLink(
      mockData.parsedURL.toBe.httpsWWW,
      mockData.parsedURL.httpsWWW,
      "./export/"
    );

    expect(spyOnExistsSync).toHaveBeenCalledWith(
      `./export/${mockData.underlineSlashed}/links/links.txt`
    );
    expect(spyOnMkdirSync).not.toHaveBeenCalledWith(
      `./export/${mockData.underlineSlashed}/links/`,
      {
        recursive: true,
      }
    );
    expect(spyOnWriteFileSync).toHaveBeenCalledWith(
      `./export/${mockData.underlineSlashed}/links/links.txt`,
      `${mockData.parsedURL.toBe.httpsWWW}\n`,
      {
        flag: "a",
      }
    );
  });
  afterEach(() => {
    jest.clearAllMocks();
    jest.restoreAllMocks();
  });
});
