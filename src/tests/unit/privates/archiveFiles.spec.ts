import archiveFiles from "../../../actions/private/archiveFiles";
import mockData from "../mocks/privates.mock";
import fs from "fs";
import { zip } from "zip-a-folder";

jest.mock("fs");
jest.mock("zip-a-folder");
jest.mock("console");

describe("Private method: archiveFiles()", () => {
  const spyOnRmdirSync = jest.spyOn(fs, "rmdirSync");
  const spyOnConsoleLog = jest.spyOn(console, "log");
  const spyOnConsoleError = jest
    .spyOn(global.console, "error")
    .mockImplementation((text: string) => {
      return text;
    });

  it("Archives the file", async () => {
    expect(spyOnRmdirSync).not.toHaveBeenCalled();
    expect(spyOnConsoleLog).not.toHaveBeenCalled();
    expect(spyOnConsoleError).not.toHaveBeenCalled();

    const mockArchiver = (zip as jest.Mock).mockImplementation(
      (srcPath: string, destPath: string) => {
        if (srcPath && destPath) {
          return true;
        }
      }
    );
    expect(mockArchiver).not.toHaveBeenCalled();

    await archiveFiles(mockData.underlineSlashed, mockData.path);

    expect(mockArchiver).toHaveBeenCalledWith(
      `${mockData.path}${mockData.underlineSlashed}/files/`,
      `${mockData.path}${mockData.underlineSlashed}/files.zip`
    );
  });
  afterEach(() => {
    jest.clearAllMocks();
    jest.restoreAllMocks();
  });
});
