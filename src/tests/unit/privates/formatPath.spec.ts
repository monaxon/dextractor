import formatPath from "../../../actions/private/formatPath";
import mockData from "../mocks/privates.mock";

describe("Private method: formatPath()", () => {
  it("Should be in correct format", () => {
    const result = formatPath(mockData.parsedURL.httpsWWW);
    return expect(result).toBe(mockData.underlineSlashed);
  });
});
