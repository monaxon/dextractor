import trimLink from "../../../actions/private/trimLink";
import mockData from "../mocks/privates.mock";

describe("Private method: trimLink()", () => {
  it("Should be in correct format", () => {
    const result1 = trimLink(
      mockData.href.withHostName,
      mockData.parsedURL.http
    );
    expect(result1).toBe(mockData.parsedURL.toBe.http.withHostName);

    const result2 = trimLink(
      mockData.href.withoutHostName,
      mockData.parsedURL.http
    );
    expect(result2).toBe(mockData.parsedURL.toBe.http.withoutHostName);

    const result3 = trimLink(
      mockData.href.withoutHostName,
      mockData.parsedURL.httpWWW
    );
    expect(result3).toBe(mockData.parsedURL.toBe.httpWWW);

    const result4 = trimLink(
      mockData.href.withoutHostName,
      mockData.parsedURL.https
    );
    expect(result4).toBe(mockData.parsedURL.toBe.https);

    const result5 = trimLink(
      mockData.href.withoutHostName,
      mockData.parsedURL.httpsWWW
    );
    expect(result5).toBe(mockData.parsedURL.toBe.httpsWWW);

    const result6 = trimLink(
      mockData.href.withoutHostName,
      mockData.parsedURL.www
    );
    expect(result6).toBe(mockData.parsedURL.toBe.www);
  });
});
