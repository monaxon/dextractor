import getHrefs from "../../../actions/private/getHrefs";
import mockData from "../mocks/privates.mock";

describe("Private method: getHrefs()", () => {
  it("Should be empty array", () => {
    const result = getHrefs("Dummy string");
    return expect(result).toHaveLength(0);
  });
  it("Should not be empty array", () => {
    const result = getHrefs(mockData.body);
    return expect(result.length).toBeGreaterThan(0);
  });
});
