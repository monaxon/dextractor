import deleteLinksIfExist from "../../../actions/private/deleteLinksIfExist";
import mockData from "../mocks/privates.mock";
import fs from "fs";

jest.mock("fs");

describe("Private method: deleteLinksIfExist()", () => {
  it("Should call fs.existsSync()", () => {
    const spy = jest.spyOn(fs, "existsSync");
    expect(spy).not.toHaveBeenCalled();
    deleteLinksIfExist(mockData.parsedURL.httpsWWW, mockData.path);
    expect(spy).toHaveBeenCalledWith(
      `${mockData.path}${mockData.underlineSlashed}/links/`
    );
  });
  it("Should call fs.rmdirSync()", () => {
    const spy = jest.spyOn(fs, "rmdirSync");
    (fs.existsSync as jest.Mock).mockReturnValue(true);
    expect(spy).not.toHaveBeenCalled();
    deleteLinksIfExist(mockData.parsedURL.httpsWWW, mockData.path);
    expect(
      spy
    ).toHaveBeenCalledWith(
      `${mockData.path}${mockData.underlineSlashed}/links/`,
      { recursive: true }
    );
  });
  afterEach(() => {
    jest.restoreAllMocks();
  });
});
