import parseLink from "../../../actions/private/parseLink";
import mockData from "../mocks/privates.mock";

describe("Private method: parseLink()", () => {
  it("Link ends in forward slash", () => {
    const result = parseLink(mockData.link.withSlash);
    return expect(result).toBe(mockData.link.withSlash);
  });
  it("Link doesn't end in forward slash", () => {
    const result = parseLink(mockData.link.withoutSlash);
    return expect(result).toBe(mockData.link.withSlash);
  });
});
