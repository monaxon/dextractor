import getBodyOfLink from "../../../actions/private/getBodyOfLink";
import mockData from "../mocks/privates.mock";
import request from "request-promise";

jest.mock("request");

describe("Private method: getBodyOfLink()", () => {
  it("Request.get gets called", () => {
    const spy = jest.spyOn(request, "get");

    expect(spy).not.toHaveBeenCalled();

    getBodyOfLink(mockData.link.withSlash);

    expect(spy).toHaveBeenCalled();
  });
  afterEach(() => {
    jest.restoreAllMocks();
  });
});
